const webpack = require('webpack');
const path = require('path');
const vendor = require('../../config/vendor');
const nodeExternalModules = require('webpack-node-externals');
const NodemonPlugin = require('nodemon-webpack-plugin')

var global = {
    bail: true,
    watch: true,
    devtool: 'cheap-module-eval-source-map',
    entry: {
        app: [
            'babel-polyfill',
            path.resolve(__filename, '../../../src/server/server.js')
        ],
    },
    output: {
        path: path.resolve(__filename, '../../../dist/server'),
        filename: ['server', 'bundle', 'js'].join('.')
    },
    target: 'async-node',
    externals: [nodeExternalModules()],
    node: {
        __filename: true,
        __dirname: true,
    },
    module: {
        rules: require('./rules'),
    }
};

var development = {
    plugins: [
        /*new NodemonPlugin({
            args: [],
            watch: path.resolve('../../../dist/server'),
            ignore: ['*.js.map'],
            verbose: true,
    })*/
    ]
};

var production = {
    plugins: [

    ]
};

module.exports = function serverConfig(env) {
    if (env === 'production') {
        return Object.assign(global, production);
    } else {
        return Object.assign(global, development);
    }
};
