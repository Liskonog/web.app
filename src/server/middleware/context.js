import {
    useStaticRendering
} from 'mobx-react';
import {
    toJS
} from 'mobx';

import state from '../../app/stores/State'
import context from '../../app/context'

const stateClone = JSON.stringify(toJS(state))
useStaticRendering(true);

export default async (ctx, resp, next) => {

    const state = JSON.parse(stateClone)
    ctx.context = context(state)

    await next();
};
