import http from 'http';
import express from 'express';
import compress from 'compression';
import logger from 'morgan';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import session from 'express-session';
import csurf from 'csurf';
import helmet from 'helmet';
import context from './middleware/context';
import ssr from './middleware/ssr';
const app = express();

app.set('host', process.env.HOST || '127.0.0.1');
app.set('port', process.env.PORT || 3000);
app.disable('x-powered-by');

app.use(helmet.frameguard());
app.use(helmet.noSniff());
app.use(helmet.xssFilter());
app.use(helmet.hsts());
app.use(compress());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cookieParser());
app.use(session({
    resave: true,
    secret: 'session@id!#98453WeRc',
    key: 'sessionID',
    saveUninitialized: true,
    cookie: {
        httpOnly: true,
    }
}));
app.use(context);
app.use(ssr);

process.on('uncaughtException', err => {
    console.error(err.message + '\n' + err.stack);
});
process.on('unhandledRejection', (reason, p) => {
    console.error("Unhandled Rejection at: Promise ", p, " reason: ", reason);
});
process.on('rejectionHandled', (reason, p) => {
    console.warn("rejectionHandled at: Promise ", p, " reason: ", reason);
});

const server = http.createServer(app);
server.listen(app.get('port'), app.get('host'), function() {
    let {
        address,
        port
    } = server.address();
    console.log(`Server listening at i http://%s:%s`, address, port);
});
