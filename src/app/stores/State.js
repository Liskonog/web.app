import {
    extendObservable,
    toJS
} from 'mobx'

class State {
    constructor(state) {
        extendObservable(this, {
            account: {
                username: null,
                token: null,
                users: []
            },
            common: {
                title: 'UpWinGO',
                statusCode: 200,
                hostname: ''
            },
            items: []
        }, state)
    }
}

export default process.env.BROWSER ? (
    window.__STATE = new State(window.__STATE)
) : new State({})
