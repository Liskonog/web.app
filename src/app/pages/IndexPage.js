import React from 'react'
import {
    Link
} from 'react-router-dom'

class IndexPage extends React.Component {
    render() {
        return (
            <div>
                    IndexPage 2
                    <Link to="/about">AboutPage</Link>
            </div>
        )
    }
}

export default IndexPage
